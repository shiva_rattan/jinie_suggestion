import csv
import json
import sys
import time,datetime,calendar
from main import get_user_data

def find_day(date):
    weekday = datetime.datetime.strptime(date, '%d %m %Y').weekday()
    return (calendar.day_name[weekday])



def find_diff(start_date,last_date):
    start_month = start_date.month
    last_month = last_date.month
    diff = last_month - start_month
    # print('Difference before in new-->', last_month, start_month)
    if diff < 0:
        diff = 12 + diff
    #print(start_date,last_date)
    #print('difff',diff)
    return diff



def threshold_values(DB_global,DB_dow,DB_dom):

    threshold_dict_global={
        1:10,
        2: 20,
        3: 30,
        4: 40,
        5: 50,
        6: 60,
        -1:sys.maxsize
    }

    threshold_dict_dow = {
        1: 2,
        2: 4,
        3: 6,
        4: 8,
        5: 10,
        6: 12,
        -1:sys.maxsize
    }
    threshold_dict_dom = {
        1: 1,
        2: 1,
        3: 2,
        4: 2,
        5: 3,
        6: 4,
        7: 5,
        8: 6,
        9: 7,
        10: 8,
        11: 9,
        -1: sys.maxsize
    }

    threshold_global=threshold_dict_global[DB_global]
    threshold_dow = threshold_dict_dow[DB_dow]
    threshold_dom = threshold_dict_dom[DB_dom]

    return threshold_global,threshold_dow,threshold_dom

def find_top_N_Intent(userId,time_range,DOW,DOM):
    Json_Resp={}

    data_boundary_global=0
    threshold_value_global = 0
    data_boundary_dow=0
    threshold_value_dow=0

    data_boundary_dom=0
    threshold_value_dom=0

    output_global=dict()
    output_DOW = dict()
    output_DOM=dict()

    output_global_list=list()
    output_DOW_list = list()
    output_DOM_list=list()

    time_range = time_range.split('-')
    start_time=time_range[0]+':00'  # adding seconds
    end_time=time_range[1]+':00'    # adding seconds
    start_time = datetime.datetime.strptime(start_time, '%H:%M:%S').time()
    end_time = datetime.datetime.strptime(end_time, '%H:%M:%S').time()
   # print(start_time,end_time,type(start_time),type(end_time))
    count_intent_freq_gloablly = dict()
    count_intent_freq_DOW = dict()
    count_intent_freq_DOM = dict()
    DAY_OF_WEEK=dict()
    DAY_OF_MONTH = dict()
    FINAL_LIST=list()
    flag_global=False
    flag_dow=False
    flag_dom=True
    last_date=''
    exp=0
    count=0
    flag_join_12=False
    index_w= 1
    join_duration=0
    while(exp<11):
        exp=exp+1
        with open('/home/shiva/PycharmProjects/Jinie_Suggestion/CSV_DATA_11/'+str(userId)+'.csv', 'r') as file:
            reader = csv.reader(file)
            next(reader)  # skip header

            for row in reader:
                apiAiCategory=row[2]
                requestDateTime=row[3]
                DateTime=datetime.datetime.strptime(requestDateTime, '%Y-%m-%d %H:%M:%S')

                if not last_date:
                    last_date=DateTime
                start_date=DateTime

                if (find_diff(start_date,last_date))<=exp:# and (find_diff(start_date,last_date))>0:
                    #print('In condition exp',start_date,'--->',last_date)
                    flag_dow=True
                    flag_global=True
                    flag_dom=True
                    #print('exp no:------',exp)
                else:
                    flag_dow = False
                    flag_global = False
                    flag_dom=False
                count=count+1

                day=DateTime.day
                month=DateTime.month
                year=DateTime.year
                date=str(day)+' '+str(month)+' '+str(year)
                day_of_week=find_day(date)
                DATE_DOW=datetime.datetime(year,month,day)
                #print(DATE_DOW,type(DATE_DOW))
                #print(type(day))
                Time=DateTime.time()
                #print(Time)

                if exp<=6:
                    if flag_global:
                        if (Time>start_time and Time<end_time):
                            #print(start_date, '--->', last_date)
                            #print(count)
                            #print('time---',Time,start_time,end_time)
                            if apiAiCategory in count_intent_freq_gloablly.keys():
                                count_intent_freq_gloablly[apiAiCategory]=count_intent_freq_gloablly[apiAiCategory]+1
                            else:
                                count_intent_freq_gloablly[apiAiCategory]=1
                            count=count+1

                    if flag_dow:
                        flag_DOW=False
                        if (Time>start_time and Time<end_time) and day_of_week==DOW:
                            #print('DOW----',DOW)
                            if apiAiCategory in count_intent_freq_DOW.keys():
                                count_intent_freq_DOW[apiAiCategory]=count_intent_freq_DOW[apiAiCategory]+1
                            else:
                                count_intent_freq_DOW[apiAiCategory]=1

                            if apiAiCategory in DAY_OF_WEEK.keys():
                                for i in DAY_OF_WEEK[apiAiCategory]:
                                    if DATE_DOW==i:
                                        flag_DOW=True

                                if not flag_DOW:
                                    #print(apiAiCategory,'->',DATE_DOW)
                                    DAY_OF_WEEK[apiAiCategory].append(DATE_DOW)
                            else:
                                DAY_OF_WEEK[apiAiCategory]=list()
                                #print(apiAiCategory, '->', date)
                                DAY_OF_WEEK[apiAiCategory].append(DATE_DOW)


                if flag_dom:
                    #print(start_date, '--->', last_date)
                    flag_DOM=False
                    if (Time>start_time and Time<end_time) and str(day)==DOM:
                        #print(start_date, '--->', last_date)
                        if apiAiCategory in count_intent_freq_DOM.keys():
                            count_intent_freq_DOM[apiAiCategory]=count_intent_freq_DOM[apiAiCategory]+1
                        else:
                            count_intent_freq_DOM[apiAiCategory]=1

                        if apiAiCategory in DAY_OF_MONTH.keys():
                            for i in DAY_OF_MONTH[apiAiCategory]:
                                if month==i:
                                    flag_DOM = True

                            if not flag_DOM:
                                #print(apiAiCategory, '------>', month)
                                DAY_OF_MONTH[apiAiCategory].append(month)
                        else:
                            DAY_OF_MONTH[apiAiCategory]=list()
                            #print(apiAiCategory,'-->',month)
                            DAY_OF_MONTH[apiAiCategory].append(month)

        if not join_duration:
            join_duration = find_diff(start_date, last_date)

        sum_gloabbly = 0
        TOP_N = 10
        global_list = list()
        for intent in sorted(count_intent_freq_gloablly, key=count_intent_freq_gloablly.get, reverse=True):
            if TOP_N > 0:
                temp_dict_global = {
                    'Intent': intent,
                    'Frequency': count_intent_freq_gloablly[intent]
                }
                # print(intent, count_intent_freq_gloablly[intent])
                TOP_N = TOP_N - 1
                global_list.append(temp_dict_global)
              #  if len(top_global_intent) < 3:
               #     top_global_intent.append({'intent': intent, 'freq': count_intent_freq_gloablly[intent]})
            sum_gloabbly = sum_gloabbly + count_intent_freq_gloablly[intent]

        sum_DOW = 0
        TOP_N = 10
        DOW_list = list()
        for intent in sorted(count_intent_freq_DOW, key=count_intent_freq_DOW.get, reverse=True):
            if TOP_N > 0:
                temp_dict_DOW = {
                    'Intent': intent,
                    'Frequency': count_intent_freq_DOW[intent]
                }
                # print('DOW',intent, count_intent_freq_DOW[intent])
                TOP_N = TOP_N - 1
                DOW_list.append(temp_dict_DOW)
            sum_DOW = sum_DOW + count_intent_freq_DOW[intent]

        DOM_list = list()
        sum_DOM = 0
        TOP_N = 10

        for intent in sorted(count_intent_freq_DOM, key=count_intent_freq_DOM.get, reverse=True):
            if TOP_N > 0:
                temp_dict_DOM = {
                    'Intent': intent,
                    'Frequency': count_intent_freq_DOM[intent]
                }
                # print(intent, count_intent_freq_DOM[intent])
                TOP_N = TOP_N - 1
                DOM_list.append(temp_dict_DOM)
            sum_DOM = sum_DOM + count_intent_freq_DOM[intent]

        count_intent_freq_gloablly={}
        count_intent_freq_DOW={}
        count_intent_freq_DOM={}

        #print(global_list)

        if len(global_list)>0 :
            output_global_list.append(global_list[0]['Intent'])
            output_global[exp]={
                'intent':global_list[0]['Intent'],
                'frequency':global_list[0]['Frequency']
            }
        else:
            output_global_list.append('null')
            output_global[exp] = {
                'intent': "",
                'frequency': 0
            }


        global_list.clear()

        #print('DOW_LIST___and exp----------------------------------------',exp,DOW_list)


        if len(DOW_list)>0:
            output_DOW_list.append(DOW_list[0]['Intent'])
            output_DOW[exp] = {
                'intent': DOW_list[0]['Intent'],
                'frequency': DOW_list[0]['Frequency']
            }
        else:
            output_DOW_list.append('null')
            output_DOW[exp] = {
                'intent':"",
                'frequency': 0
            }
            index_w=index_w+1

        DOW_list.clear()

        if len(DOM_list)>0:
            output_DOM_list.append(DOM_list[0]['Intent'])
            output_DOM[exp] = {
                'intent': DOM_list[0]['Intent'],
                'frequency': DOM_list[0]['Frequency']
            }
        else:
            output_DOM_list.append('null')
            output_DOM[exp] = {
                'intent':"",
                'frequency': 0
            }

        #print('DOM_LIST___',exp, DOM_list)

        DOM_list.clear()

        if join_duration<=exp:
            break


    print(join_duration)
    print('DATE RANGE',start_date,last_date)
    print('**************Globally*************')


    if join_duration>=7:
        for i in range(7,join_duration+1):
            del output_global[i]
            del output_DOW[i]



    print('OUTPUT GLOBAL')
    print(json.dumps(output_global,indent=4,default=str))

    print('OUTPUT DOW')
    print(json.dumps(output_DOW, indent=4, default=str))

    print('OUTPUT DOM')
    print(json.dumps(output_DOM, indent=4, default=str))

    if len(output_global_list)>0:
        if output_global_list[0]!='null':
            event_global=output_global_list[0]
            data_boundary_global=1
            for j in range(1,len(output_global_list)):
                if event_global==output_global_list[j]:
                    #print('********',event_global,output_global_list[j])
                    data_boundary_global=data_boundary_global+1
                else:
                    break
        else:
            data_boundary_global=-1

    #print('Global',event_global,data_boundary_global)
    print('output dow list------',output_DOW_list)
    #print('output DOM list------', output_DOM_list)


    if len(output_DOW_list)>0:
        if output_DOW_list[0]!='null':
            event_dow=output_DOW_list[0]
            data_boundary_dow=1
            for j in range(1,len(output_DOW_list)):
                if event_dow==output_DOW_list[j]:
                    data_boundary_dow=data_boundary_dow+1
                else:
                    break
        else:
            data_boundary_dow=-1

    if len(output_DOM_list)>0:
        if output_DOM_list[0]!='null':
            event_dom=output_DOM_list[0]
            data_boundary_dom=1
            for j in range(1,len(output_DOM_list)):
                if event_dom==output_DOM_list[j]:
                    data_boundary_dom=data_boundary_dom+1
                else:
                    break
        else:
            data_boundary_dom=-1


    #print('DOW',event_dow,data_boundary_dow)

    print('Data boundaries are ',data_boundary_global,data_boundary_dow,data_boundary_dom)
    threshold_value_global,threshold_value_dow,threshold_value_dom=threshold_values(data_boundary_global,data_boundary_dow,data_boundary_dom)
    print('Threshold values are ',threshold_value_global,threshold_value_dow,threshold_value_dom)

    if data_boundary_global in output_global.keys():
        freq_global=output_global[data_boundary_global]['frequency']
        print('freq global',freq_global)
        if freq_global>threshold_value_global:
            print('added from global')
            FINAL_LIST.append(event_global)
    else:
        print('nothing added from Global')



    if data_boundary_dow in output_DOW.keys():
        freq_dow=output_DOW[data_boundary_dow]['frequency']
        print('freq dow',freq_dow)
        if freq_dow>threshold_value_dow:
            if event_dow not in FINAL_LIST:
                print('added from DOW')
                FINAL_LIST.append(event_dow)
    else:
        print('Nothing to add from DOW')

    if data_boundary_dom in output_DOM.keys():
        freq_dom = output_DOM[data_boundary_dom]['frequency']
        print('freq dom', freq_dom)
        if freq_dom > threshold_value_dom:
            if event_dom not in FINAL_LIST:
                print('added from DOM')
                FINAL_LIST.append(event_dom)
    else:
        print('Nothing to add from DOM')



    print('*************FINAL RESPONSE****************')

    if(len(FINAL_LIST))>0:
        return FINAL_LIST
    return "NOTHING TO SUGGEST AS OF NOW"

if __name__=="__main__":

    userId = 2060312 #4317990 #4317995 #2060312 #1749823 #2205395
    time_range = '12:00-18:00'
    DOW = 'Monday'
    DOM = '28'

    print()
    print(find_top_N_Intent(userId,time_range,DOW,DOM))