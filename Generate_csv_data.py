import MySQLdb
import csv

SQL_CREDENTIALS = {"IP": "10.226.0.200", "USER": "ZippiServices", "PASSWORD": "Zipp!$073s",
                   "DB": "TalentPactChatMaster"}

fields=['OrgId','userid','apiAiCategory','requestDateTime']



def connect_to_sql_server():
    """
    Connect to MS-SQL server
    :return: Connection object
    """
    crsr = None
    try:
        sql_connection_client = MySQLdb.connect(SQL_CREDENTIALS["IP"],
                                                SQL_CREDENTIALS["USER"],
                                                SQL_CREDENTIALS["PASSWORD"],
                                                SQL_CREDENTIALS["DB"])
        crsr = sql_connection_client.cursor()
        print(crsr)

    except Exception as ex:
        print(ex)
    return crsr

def get_user_data(userId):
    time='2020-04-01 00:00:00'
    crsr = connect_to_sql_server()
    query = "select orgId, userid, apiAiCategory, requestDateTime from JinieTracker jt where apiAiCategory is not NULL and requestDateTime>%s and userid=%s order by requestDateTime desc limit 10000000;"
    crsr.execute(query, (time,userId,))
    result = crsr.fetchall()
    data_list = list()
    if len(result)!=0:
        temp=dict()

        for u in result:
            temp={

            'OrgId':u[0],
            'userid':u[1],
            'apiAiCategory':u[2],
            'requestDateTime':u[3]

            }
            data_list.append(temp)
            temp={}

        f = open('/home/shiva/PycharmProjects/Jinie_Suggestion/CSV_DATA_11/' + str(userId) + '.csv', 'w')
        filename = '/home/shiva/PycharmProjects/Jinie_Suggestion/CSV_DATA_11/{}.csv'.format(userId)
        print(filename)
        with open(filename, 'w') as csvfile:
            writer = csv.DictWriter(csvfile, fieldnames=fields)
            writer.writeheader()
            writer.writerows(data_list)
        return True
    else:
        print('Sorry,No DATA exist with this user ID')
        return False


if __name__=="__main__":
    data_list=get_user_data(3218689)
    print(data_list)
